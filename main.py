import asyncio
import requests
import json
import os
import logging
from telegram import Bot
from logging.handlers import RotatingFileHandler
from dotenv import load_dotenv

# Загрузка переменных окружения
dotenv_path: str = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

config_file: str = os.getenv('CONFIG_FILE', 'config.json')
mapping_file: str = os.getenv('MAPPING_FILE', 'mapping.json')
telegram_token: str = os.getenv('TELEGRAM_TOKEN')
bot = Bot(token=telegram_token)


def load_json_file(file_path: str) -> dict:
    """
    Загружает JSON-файл и возвращает его содержимое.
    :param file_path: Путь к файлу.
    :return: Содержимое файла JSON.
    """
    with open(file_path, 'r') as file:
        return json.load(file)


def find_delivery_channel(tag: str, mappings: dict) -> str:
    """
    Находит канал доставки для заданного тега.
    :param tag: Тег сервиса.
    :param mappings: Словарь с маппингом тегов и каналов доставки.
    :return: ID канала доставки.
    """
    try:
        for tag_mapping in mappings['tags']:
            if tag_mapping['name'] == tag:
                return os.getenv(tag_mapping['delivery_channel'])
        return os.getenv('DEFAULT_TELEGRAM_CHAT_ID')
    except KeyError:
        logging.error(f"Tag not found in mappings: {tag}")
        return os.getenv('DEFAULT_TELEGRAM_CHAT_ID')


def setup_logging(config: dict) -> None:
    """
    Настраивает параметры логгирования.
    :param config: Конфигурация скрипта.
    """
    log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_format)

    logging_config: str = config.get('logging', {})
    logging_type: str = logging_config.get('logging_type', 'stdout')
    log_path: str = logging_config.get('path', '')

    if logging_type == 'file' and log_path:
        # Файл до 10 Мб и таких максимум 5 файлов
        handler = RotatingFileHandler(log_path, maxBytes=10485760, backupCount=5)
        handler.setFormatter(logging.Formatter(log_format))
        logging.getLogger('').addHandler(handler)


async def send_notification(service: dict, response: requests.Response) -> None:
    """
    Отправляет уведомление о статусе сервиса.
    :param service: Информация о сервисе.
    :param response: Ответ от сервиса.
    """
    try:
        chat_id: str = find_delivery_channel(service.get('tag', ''), mappings)
        message: str = f"Service {service['name']} [{service.get('tag', 'no tag')}] responded with status: {response}"
        logging.warning(message)
        await bot.send_message(chat_id=chat_id, text=message)
    except Exception as e:
        logging.error(f"Error sending notification for {service['name']}: {e}")


async def check_service_status(service: dict) -> None:
    """
    Проверяет статус заданного сервиса.
    :param service: Информация о сервисе.
    """
    url = f"{service['address']}:{service['port']}{service['path']}"
    try:
        response = requests.get(url)
        if response.status_code != 200:
            await send_notification(service, response)
    except requests.RequestException as e:
        await send_notification(service, str(e))


async def main() -> None:
    """
    Главная функция, управляющая процессом проверки сервисов.
    """
    setup_logging(config)
    check_interval = int(config.get('check_interval', 60))

    while True:
        await asyncio.gather(*(check_service_status(service) for service in config['services']))
        await asyncio.sleep(check_interval)


if __name__ == "__main__":
    config: dict = load_json_file(config_file)
    mappings: dict = load_json_file(mapping_file)
    asyncio.run(main())
